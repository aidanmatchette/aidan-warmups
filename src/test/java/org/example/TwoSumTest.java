package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TwoSumTest {
    private TwoSum ts;

    @BeforeEach
    void setUp() {
        ts = new TwoSum();
    }

    @Test
    void returnNegativeOneArrayForNoAnswerFound() {
        int[] nums = new int[] {1,2,3,4,5};


        int[] actual = ts.solution(nums, 2);
        int[] expected = new int[] {-1, -1};

        assertArrayEquals(new int[] {-1, -1}, actual);
    }


    @Test
    void givenAnArrayFindTwoNumsThatAddToTarget() {

        int[] nums = new int[] {1,2,3,5,7};

        int[] actual = ts.solution(nums, 9);

        int[] expected = new int[] {1,4};

        assertArrayEquals(expected, actual);
    }

    @Test
    void solutionWithEmptyArgs() {

        int[] nums = new int[] {};

        int[] actual = ts.solution(nums, 1);
        int[] expected = new int[] {-1,-1};

        assertArrayEquals(expected, actual);
    }
}